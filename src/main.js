import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

const init = () =>{
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
};

document.addEventListener("deviceready", () => {
  console.log("Ready, Render the App");
  init();
});

const isCordovaApp = (typeof window.cordova !== "undefined");
if (!isCordovaApp){
  document.dispatchEvent(new CustomEvent("deviceready", {}));
}
